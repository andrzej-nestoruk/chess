# CHESS

## Starting

Running application:

```
sbt run
```


To run test execute:

```
sbt test
```

Important setting from `application.conf` file is `threshold` parameter. 

It which indicates how many combinations can be stored in memory before they are dumped into temporary file. Default value is **250000**, minimum accepted value is **20000**. On machines with more RAM available increasing this value should improve performance slightly.

## Results

For Input:

```
M (rows) = 7
N (columns) = 7
Kings = 2
Queens = 2
Rooks = 0
Knights = 1
Bishops = 2
```

Mine results was:

```
Total number of combinations: 3063828
Took: 176.66 seconds

Total number of combinations: 3063828
Took: 196.025 seconds

Total number of combinations: 3063828
Took: 202.932 seconds
```