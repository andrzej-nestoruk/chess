package pl.anestoruk.models

/**
 * Used to hold coordinates
 */
case class Position(x: Int, y: Int)

trait Field

case class Fields(data: Seq[Seq[Field]])

case object EmptyField extends Field {
  override def toString = " "
}