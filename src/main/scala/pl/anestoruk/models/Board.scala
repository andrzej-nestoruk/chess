package pl.anestoruk.models

abstract class Board() {
  
  val rows: Int
  val cols: Int
  val fields: Fields
  
  /**
   * Flat sequence of Positions available for this Board
   */
  lazy val positions = for {
    row <- (0 to rows - 1)
    col <- (0 to cols - 1)
  } yield Position(row, col)
  
  /**
   * Flat sequence of Positions that are attacked by existing Pieces
   */
  lazy val attackedPositions = (for {
      p <- positions
    } yield {
      fields.data(p.x)(p.y) match {
        case piece: Piece => 
          piece.attacks(p, this)
        case _ => 
          Seq.empty
      }
    }).flatten.distinct
  
  /**
   * Gets Field for specific Position
   */
  def getField(position: Position): Option[Field] = 
    if (isPosValid(position))
      Some(fields.data(position.x)(position.y))
    else
      None
    
  /**
   * Checks if provided Position is within Board's boundaries
   */
  def isPosValid(position: Position): Boolean = 
    position.x >= 0 && position.x < rows && position.y >= 0 && position.y < cols
  
  def isNotAttacked(position: Position): Boolean =
    isPosValid(position) && !attackedPositions.contains(position)
  
  def isPosFree(position: Position): Boolean =
    getField(position).map { 
      _ match {
        case EmptyField => 
          true
        case _ => 
          false
      }
    }.getOrElse { true }
  
  def isAttackPosValid(piece: Piece, position: Position) = 
    piece.attacks(position, this).forall { isPosFree(_) }
  
  def canBeInserted(piece: Piece, position: Position) = 
    isNotAttacked(position) && isAttackPosValid(piece, position)
  
  /**
   * Custom 'equals' method. Board objects are compared by their Fields sequences.
   */
  override def equals(o: Any) = o match {
    case that: Board => 
      this.fields.data.flatten.equals(that.fields.data.flatten)
    case _ => 
      false 
  }
  
  /**
   * Overridden to match new 'equals' method
   */
  override def hashCode = this.fields.data.flatten.map { _.hashCode }.##
  
  /**
   * Used to 'serialize' Board object into short String
   */
  override def toString = this.fields.data.map { _.mkString }.mkString(";")
  
}


object Board {
  
  /**
   * Creates Board with already defined Fields
   */
  def apply(_fields: Fields) = 
    new {
     val fields = _fields
     val rows = fields.data.length
     val cols = fields.data.head.length
    } with Board
  
  /**
   * Creates Board filled with EmptyFields
   */
  def apply(_rows: Int, _cols: Int) = 
    new {
      val rows = _rows
      val cols = _cols
      val fields = Fields(Seq.fill(rows, cols)(EmptyField))
    } with Board
    
}