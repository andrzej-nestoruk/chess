package pl.anestoruk.models

import pl.anestoruk.core.PositionFinder

/**
 * All Chess Pieces should extends this trait
 */
trait Piece extends Field with PositionFinder {
  
  /**
   * Returns sequence of Positions that are being attacked by this Piece on specific Board
   */
  def attacks(position: Position, board: Board): Seq[Position]
  
}


case object King extends Piece {
  
  override def toString = "K"
  
  def attacks(position: Position, board: Board): Seq[Position] =
    getSurroundingFields(position, board.rows, board.cols).distinct
  
}


case object Queen extends Piece {
  
  override def toString = "Q"
  
  def attacks(position: Position, board: Board): Seq[Position] = {
    val rowFields = getRowFields(position.x, board.cols)
    val colFields = getColFields(position.y, board.rows)
    val diagFields = getDiagonalFields(position, board.rows, board.cols)
    (rowFields ++ colFields ++ diagFields).distinct
  }
  
}


case object Rook extends Piece {
  
  override def toString = "R"
  
  def attacks(position: Position, board: Board): Seq[Position] = {
    val rowFields = getRowFields(position.x, board.cols)
    val colFields = getColFields(position.y, board.rows)
    (rowFields ++ colFields).distinct
  }
  
}


case object Knight extends Piece {
  
  override def toString = "N"
  
  def attacks(position: Position, board: Board): Seq[Position] = 
    getKnightJumpFields(position, board.rows, board.cols)
  
}


case object Bishop extends Piece {
  
  override def toString = "B"
  
  def attacks(position: Position, board: Board): Seq[Position] = 
    getDiagonalFields(position, board.rows, board.cols)
  
}