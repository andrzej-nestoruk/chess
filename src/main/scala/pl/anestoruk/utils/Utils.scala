package pl.anestoruk.utils

import scala.io.StdIn
import scala.math.BigInt.int2bigInt
import scala.util.{ Failure, Success, Try }
import pl.anestoruk.models.Position

/**
 * Additional helper methods
 */
object Utils {
  
  /**
   * Implicit conversion for tuple Integer into Position
   */
  implicit def tuple2Position(tuple: (Int, Int)): Position = 
    Position(tuple._1, tuple._2)
  
  /**
   * Reads Integer from Std Input
   */
  def getInt(msg: String): Int = {
    Try(StdIn.readLine(msg).toInt) match {
      case Success(i) if i >= 0 => i
      case _ => 0
    }
  }
  
  /**
   * Calculates factorial of specified number
   */
  def factorial(n: Int): BigInt = {
    if (n < 0) throw new ArithmeticException
    (1 to n).foldLeft(1: BigInt) { _ * _ }
  }
  
  /**
   * Calculates variation for provided 'n' and 'k'.
   * 
   * {{{
   * V(n, k) = n! / (n - k)!
   * }}}
   */
  def variation(n: Int, k: Int) = factorial(n) / factorial(n - k)
  
  /**
   * Merges sequence of Iterators into single Iterator that emits sorted values.
   */
  def mergeAndSort[T](iterators: Seq[Iterator[T]])(implicit ord: Ordering[T]): Iterator[T] = {
    val streams = iterators.map { _.toStream }
    
    def merge(data: Seq[Stream[T]]): Stream[T] = {
      val filtered = data
        .filter { !_.isEmpty }
      
      val sorted = filtered
        .map { stream => (stream.head, stream) }
        .sortBy { case (head, _) => head }
        .map { _._2 }
      
      if (!sorted.isEmpty) {
        val first = sorted.head
        val rest = sorted.drop(1) :+ first.tail
        (first.head #:: merge(rest))
      } else {
        Stream.empty
      }
    }
    
    merge(streams).toIterator
  }
}