package pl.anestoruk.utils
import com.typesafe.config.ConfigFactory
/** * Mix in this trait for simple access to application.conf settings */
trait Config {
  
  private val config = ConfigFactory.load()
  val settings = config.getConfig("settings");  
}