package pl.anestoruk.core

import scala.annotation.tailrec
import pl.anestoruk.models._
import pl.anestoruk.utils.Config
import pl.anestoruk.utils.Utils.tuple2Position
import scala.collection.immutable.SortedSet

trait Logic extends Config {
  
  /**
   * 
   */
  def range(board: Board): Iterator[Position] =
    for {
      x <- (0 to board.rows - 1).iterator
      y <- (0 to board.rows - 1).iterator
    } yield Position(x, y)
  
  /**
   * Main function, it finds all combinations of Chess Pieces that can be placed 
   * on provided Board.
   */  
  def findCombinations(rows: Int, cols: Int, pieces: Seq[Piece]): Stream[String] = {
    println("\nWorking...\n")
    
    @tailrec
    def loop(input: Iterator[String], pieces: Iterator[Piece]): Stream[String] = {
      println(".")
      if (pieces.hasNext)
        loop(consumeInput(input, pieces.next).iterator, pieces)
      else
        input.toStream
    }
    
    pieces match {
      case head :: tail => loop(Iterator(Board(rows, cols).toString), pieces.iterator)
      case Nil => Stream.empty[String]
    }
  }
  
  /**
   * 
   */
  private def consumeInput(iterator: => Iterator[String], piece: Piece): SortedSet[String] = {
    @tailrec
    def consumeInput(iter: Iterator[String], out: SortedSet[String] = SortedSet.empty): SortedSet[String] = {
      if (!iter.hasNext) out
      else {
        val maybeSet = deserialize(iter.next).map { board => consumeRange(board, piece) }
        if (maybeSet.isDefined) 
          consumeInput(iter, out ++ maybeSet.get)
        else 
          consumeInput(iter, out)
      }
    }
    consumeInput(iterator)
  }
  
  /**
   * 
   */
  private def consumeRange(board: Board, piece: Piece): SortedSet[String] = {
    val iterator = range(board)
    @tailrec
    def consumeRange(iter: Iterator[Position], out: SortedSet[String] = SortedSet.empty): SortedSet[String] = {
      if (!iter.hasNext) out
      else {
        val maybeBoard = insert(board, piece, iter.next).map { _.toString }
        if (maybeBoard.isDefined) 
          consumeRange(iter, out + maybeBoard.get)
        else 
          consumeRange(iter, out)
      }
    }
    consumeRange(iterator)
  }
  
  /**
   * Deserializes String into Board if provided input is valid
   */
  def deserialize(input: String): Option[Board] = {
    val optFields = input.split(";").toSeq.map { line =>
      line.map { c =>
        val field: Option[Field] = c match {
          case 'K' => Some(King)
          case 'Q' => Some(Queen)
          case 'R' => Some(Rook)
          case 'N' => Some(Knight)
          case 'B' => Some(Bishop)
          case ' ' => Some(EmptyField)
          case _ => None
        }
        field
      }
    }
    if (!optFields.flatten.contains { None } && optFields.flatten.length > 0)
      Some(Board(Fields(optFields.map { _.flatten })))
    else 
      None
  }
  
  /**
   * Insert (if possible) provided Piece at Position on Board and returns new Board on success
   */
  def insert(board: Board, piece: Piece, position: Position): Option[Board] = {
    if (board.canBeInserted(piece, position)) {
      val updatedRow = board.fields.data(position.x)
        .updated(position.y, piece)
      val result = board.fields.data
        .updated(position.x, updatedRow)
      Some(Board(Fields(result)))
    } else {
      None
    }
  }
  
  /**
   * Prints Board's Fields into console output 
   */
  def show(board: Board): Unit = {
    def print(p: Position) = {
      board.getField(p).map {
        _ match {
          case EmptyField if (!board.isNotAttacked(p)) => 
            " + "
          case field @ _ => 
            s" ${field.toString} "
        }
      }.getOrElse("")
    }
    val line = List.fill(board.cols)("----").mkString + "-"
    (0 to board.rows - 1).foreach { x =>
      val mid = (0 to board.cols - 1)
        .map { y => "|" + print((x, y)) }.mkString + "|"
      println(line)
      println(mid)
    }
    println(line)
    println
  }
  
}