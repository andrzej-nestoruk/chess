//package pl.anestoruk.core
//
//import java.io.ByteArrayInputStream
//import java.nio.file.{ Files, OpenOption, Path, Paths }
//import java.nio.file.StandardOpenOption._
//import scala.collection.JavaConversions._
//import scala.collection.immutable.SortedSet
//import scala.io.{ BufferedSource, Source }
//import scala.math.BigInt.long2bigInt
//import scala.util.Try
//import pl.anestoruk.models.Board
//import pl.anestoruk.utils.Utils.{ mergeAndSort, variation }
//
//class BoardBuffer(val initialBoard: Board) extends Traversable[String] with Logic {
//  
//  private[core] var resultBuffer: SortedSet[String] = SortedSet.empty
//  
//  private[core] var tmpBuffer: SortedSet[String] = SortedSet.empty
//  
//  initialize
//  
//  /**
//   * NOTE: Source needs to be wrapped with try-finally because otherwise methods like 'head' or 'last'
//   * will break out of 'foreach' without closing the source!
//   */
//  def foreach[U](f: String => U): Unit = {
//    val source = resultSource
//    try {
//      val iterator = source.getLines
//      for (line <- iterator) f(line)
//    } finally {
//      source.close
//    }
//  }
//  
//  /**
//   * Adds (unique) Board to buffer
//   */
//  def +=(board: Board): Unit = {
//    tmpBuffer += board.toString
//  }
//  
//  /**
//   * Flushes temporary data into result data
//   */
//  def flush: Unit = {
//    resultBuffer = tmpBuffer
//    tmpBuffer = SortedSet.empty
//  }
//  
//  /**
//   * Stores initial Board
//   */
//  protected def initialize: Unit = {
//    resultBuffer += initialBoard.toString
//  }
//  
//  /**
//   * Prepares source for 'foreach' method
//   */
//  protected def resultSource: BufferedSource = {
//    val bytes = (resultBuffer.mkString("\n") + "\n").getBytes
//    val stream = new ByteArrayInputStream(bytes)
//    Source.fromInputStream(stream)
//  }
//
//  /**
//   * Flushes current buffer state into single string
//   */
//  protected def flushBuffer: String = {
//    val result = tmpBuffer.mkString("\n") + "\n"
//    tmpBuffer = SortedSet.empty
//    result
//  }
//  
//}
//
//case class BoardBufferConfig(
//    directory: String,
//    resultFile: String,
//    tmpFile: String,
//    threshold: Long)
//
///**
// * Expanded version of BoardBuffer, it is using temporary files to store partial results when they does not fit
// * into memory.
// */
//class FileBoardBuffer(initialBoard: Board, cfg: BoardBufferConfig) extends BoardBuffer(initialBoard) {
//  
//  /**
//   * Counts how many times threshold has been "broken"
//   */
//  private[core] var breaks: Int = 0
//  
//  def threshold: Long = cfg.threshold
//  
//  def resultPath: Path = Paths.get(cfg.directory + cfg.resultFile)
//  
//  def tmpPath(i: Int): Path = Paths.get(cfg.directory + cfg.tmpFile + i)
//  
//  override def +=(board: Board): Unit = {
//    tmpBuffer += board.toString
//    if (tmpBuffer.size >= threshold) {
//      println("Threshold reached! Saving...")
//      writeFile(tmpPath(breaks), flushBuffer)
//      breaks += 1
//    }
//  }
//  
//  override def flush: Unit = {
//    println("Flushing data...")
//    mergeTmpFiles.getOrElse {
//      writeFile(resultPath, flushBuffer)
//    }
//    deleteAllTmpFiles
//    breaks = 0
//  }
//  
//  override protected def initialize: Unit = {
//    setupFiles(resultPath.getParent)
//    writeFile(resultPath, initialBoard.toString)
//  }
//  
//  override protected def resultSource: BufferedSource = readFile(resultPath)
//  
//  /**
//   * Merges temporary files into final result file. All possible duplications should be removed here
//   */
//  private def mergeTmpFiles: Option[Unit] = {
//    if (breaks > 0) {
//      println("Merging temporary chunks...")
//      
//      writeFile(tmpPath(breaks), flushBuffer)
//      writeFile(resultPath, "")
//      
//      // prepare iterator for every tmp file
//      val sources = for (i <- 0 to breaks) yield readFile(tmpPath(i))
//      val iterator = mergeAndSort(sources.map { _.getLines })
//      
//      // after each file write, last element from buffer is stored to ensure nothing will be duplicated
//      var last = ""
//      for (line <- iterator) {
//        tmpBuffer += line
//        if (tmpBuffer.size > threshold) {
//          tmpBuffer -= last
//          last = tmpBuffer.last
//          writeFile(resultPath, flushBuffer, APPEND)
//        }
//      }
//      sources.foreach { _.close }
//      
//      tmpBuffer -= last
//      writeFile(resultPath, flushBuffer, APPEND)
//      
//      Some()
//    } else {
//      None
//    }
//  }
//  
//  /**
//   * Creates files (or recreates them, if they already exist!) specified in paths.
//   */
//  private def setupFiles(dir: Path) = Try {
//    if (!Files.exists(dir))
//      Files.createDirectories(dir)
//      
//    Files.list(dir).iterator.foreach { p =>
//      Files.deleteIfExists(p)
//    }
//  }
//  
//  private def deleteAllTmpFiles = Try {
//    (0 to breaks).foreach { i =>
//      Files.deleteIfExists(tmpPath(i))
//    }
//  }
//  
//  private def readFile(path: Path) = {
//    Source.fromFile(path.normalize.toString)
//  }
//  
//  private def writeFile(path: Path, data: String, opts: OpenOption*) = Try {
//    Files.write(path, data.getBytes, opts:_*)
//  }
//  
//}
//
//
//object BoardBuffer {
//  
//  /**
//   * Factory method
//   */
//  def apply(rows: Int, cols: Int, piecesLength: Int, config: BoardBufferConfig): BoardBuffer = {
//    val emptyBoard = Board(rows, cols)
//    val complexity = variation(rows * cols, piecesLength)
//    val threshold = Math.max(config.threshold, 20000)
//    val validConfig = config.copy(threshold = threshold)
//    
//    (complexity > validConfig.threshold) match {
//      case true => new FileBoardBuffer(emptyBoard, validConfig) 
//      case _ => new BoardBuffer(emptyBoard)
//    }
//  }
//  
//}