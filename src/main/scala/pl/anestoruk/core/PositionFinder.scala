package pl.anestoruk.core

import pl.anestoruk.models.Position

trait PositionFinder {
  
  /**
   * Gets sequence of Positions for specific row
   * 
   * @param row index
   * @param cols number of columns, this will be the size of sequence
   */
  def getRowFields(row: Int, cols: Int): Seq[Position] = 
    Seq.fill(cols)(row)
      .zipWithIndex
      .map { case (x, y) => Position(x, y) }
  
  /**
   * Gets sequence of Positions for specific column
   * 
   * @param col index
   * @param rows number of rows, this will be the size of sequence
   */
  def getColFields(col: Int, rows: Int): Seq[Position] = 
    Seq.fill(rows)(col)
      .zipWithIndex
      // NOTE: we are reversing coordinates here!
      .map { case (y, x) => Position(x, y) }
  
  /**
   * Gets diagonal fields for specific Position
   * 
   * @param position starting Position
   * @param rows number of rows
   * @param cols number of columns
   */
  def getDiagonalFields(position: Position, rows: Int, cols: Int): Seq[Position] = {
    val stream = Stream.from(1)
    val iter1, iter2, iter3, iter4 = stream.iterator
    val result = (Seq.fill(cols)(position.x - iter2.next)
      .zipWithIndex
      .map { case(x, idx) => Position(x, position.y - 1 - idx) }) ++
    (Seq.fill(cols)(position.x + iter1.next)
      .zipWithIndex
      .map { case(x, idx) => Position(x, position.y + 1 + idx) }) ++
    (Seq.fill(cols)(position.x - iter3.next)
      .zipWithIndex
      .map { case(x, idx) => Position(x, position.y + 1 + idx) }) ++
    (Seq.fill(cols)(position.x + iter4.next)
      .zipWithIndex
      .map { case (x, idx) => Position(x, position.y - 1 - idx) }) :+ position
      
    result
      .filter { p => 
        (p.x >= 0 && p.x < rows) && (p.y >= 0 && p.y < cols) 
      }
  }
  
  /**
   * Gets fields around specific Position
   * 
   * @param position starting Position, included in result sequence!
   * @param rows
   * @param cols
   */
  def getSurroundingFields(position: Position, rows: Int, cols: Int): Seq[Position] = {
    (for {
      x <- (position.x - 1 to position.x + 1)
      y <- (position.y - 1 to position.y + 1)
    } yield Position(x, y)).filter { p => 
      (p.x >= 0 && p.x < rows) && (p.y >= 0 && p.y < cols) 
    }
  }
  
  /**
   * Gets fields attacked by Knight jump
   * 
   * @param position Knight's Position, included in result sequence!
   * @param rows
   * @param cols
   */
  def getKnightJumpFields(position: Position, rows: Int, cols: Int): Seq[Position] = {
    val list = List(-2, -1, 1, 2)
    
    val result = for {
      x <- list
      y <- list if (Math.abs(x) != Math.abs(y))
    } yield (x, y)
    
    result.map { case (x, y) =>
      Position(position.x + x, position.y + y)
    } :+ position
  }
  
}