package pl.anestoruk

import pl.anestoruk.core.Logic
import pl.anestoruk.models._
import pl.anestoruk.utils.Config
import pl.anestoruk.utils.Utils._

object Main extends Logic with Config {
  
  def main(args: Array[String]) = {
    
    // get Input
    val rows = getInt("M (rows) = ")
    val cols = getInt("N (columns) = ")
    val kings = getInt("Kings = ")
    val queens = getInt("Queens = ")
    val rooks = getInt("Rooks = ")
    val knights = getInt("Knights = ")
    val bishops = getInt("Bishops = ")
    
    // create Piece(s)
    val pieces: Seq[Piece] = 
      Seq.fill(queens)(Queen) ++ 
      Seq.fill(rooks)(Rook) ++ 
      Seq.fill(bishops)(Bishop) ++ 
      Seq.fill(knights)(Knight) ++ 
      Seq.fill(kings)(King)
      
    // if there are more Pieces than Fields return with error
    if (pieces.length > rows * cols) {
      println(s"\nNumber of Pieces (${pieces.length}) can't be higher than total number of Board fields (${rows * cols})! Please try again with proper input.\n")
      System.exit(1)
    }
    
    val startTime = System.currentTimeMillis
    val stream = findCombinations(rows, cols, pieces)
    val endTime = System.currentTimeMillis
    val length = stream.length
    println("Done!")
    
    if (length < 10)
      stream.foreach(deserialize(_).map(show))
    else 
      (stream.head :: stream.last :: Nil).foreach(deserialize(_).map(show))
    
    println(s"Total number of combinations: ${length}")
    println(s"Took: ${((endTime - startTime) / 1000.toFloat)} seconds")
    
  }
  
}