package pl.anestoruk.models

import org.scalatest._
import pl.anestoruk.core.Logic
import pl.anestoruk.utils.Utils.tuple2Position
import pl.anestoruk.core.PositionFinder

class PositionFinderSpec extends WordSpec with Matchers {
  
  trait PieceContext extends PositionFinder {
    val order: Position => (Int, Int) = p => (p.x, p.y)
  }
  
  "PositionFinder" when {
    
    "getRowFields method is invoked" should {
      "generate valid sequence of Positions for specified row" in new PieceContext {
        val expected: Seq[Position] = 
          Seq((2, 0), (2, 1), (2, 2), (2, 3), (2, 4))
          
        getRowFields(2, 5) shouldBe expected
      }
    }
    
    "getColFields method is invoked" should {
      "generate valid sequence of Positions for specified column" in new PieceContext {
        val expected: Seq[Position] = 
          Seq((0, 1), (1, 1), (2, 1), (3, 1))
          
        getColFields(1, 4) shouldBe expected
      }
    }
    
    "getDiagonalFields method is invoked" should {
      "generate valid sequence of Positions that are diagonal to specifiec Position" in new PieceContext {
        val expected: Seq[Position] = 
          Seq((1, 0), (3, 0), (1, 2), (3, 2), (0, 3), (2, 1))
        
        getDiagonalFields((2, 1), 4, 4).sortBy(order) shouldBe expected.sortBy(order)
      }
    }
    
    "getSurroundingFields method is invoked" should {
      "generate valid sequence of Positions surrounding specified Position" in new PieceContext {
        val expected: Seq[Position] = Seq(
              (1, 2), (1, 3), (1, 4),
              (2, 2), (2, 3), (2, 4),
              (3, 2), (3, 3), (3, 4))
          
          getSurroundingFields((2, 3), 5, 5).sortBy(order) shouldBe expected.sortBy(order)
      }
    }
    
    "getKnightJumpFields method is invoked" should {
      "generate valid sequence of Knight-jump Positions for specified Position" in new PieceContext {
        val expected: Seq[Position] = Seq(
            (0, 1), (0, 3),
            (1, 0), (1, 4),
            (3, 0), (3, 4),
            (4, 1), (4, 3),
            (2, 2))
        
        getKnightJumpFields((2, 2), 5, 5).sortBy(order) shouldBe expected.sortBy(order)
      }
    }
    
  }
  
}