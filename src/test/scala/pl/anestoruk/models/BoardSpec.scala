package pl.anestoruk.models

import org.scalatest._
import pl.anestoruk.core.Logic
import pl.anestoruk.utils.Utils.tuple2Position

class BoardSpec extends WordSpec with Matchers {
  
  trait EmptyBoard {
    val rows = 4
    val cols = 3
    val board = Board(rows, cols)
  }
  
  
  trait RooksBoard extends EmptyBoard with Logic {
    val emptyBoard = Board(rows, cols)
    
    /**
     * Board will look like this:
     * 
     * {{{
     * |R|+|+|
     * |+|R|+|
     * |+|+| |
     * |+|+| |
     * }}}
     */
    override val board = (for {
      board <- insert(emptyBoard, Rook, (0, 0))
      board <- insert(board, Rook, (1, 1))
    } yield board).get
    
  }
  
  
  "Empty Board" should {
    
    "have correct number of Positions" in new EmptyBoard {
      board.positions.length shouldBe rows * cols
    }
    
    "have corrent number of Fields" in new EmptyBoard {
      board.fields.data.flatten.length shouldBe rows * cols
    }
    
    "return Field on existing Position" in new EmptyBoard {
      board.getField((0, 0)) shouldBe Some(EmptyField)
      board.getField((-1, -1)) shouldBe None
    }
    
  }
  
  
  "Board with Rooks" should {
    
    "have Pieces on proper Positions" in new RooksBoard {
      board.getField((0, 0)) shouldBe Some(Rook)
      board.getField((0, 1)) shouldBe Some(EmptyField)
    }
    
    "return correct attacked Positions" in new RooksBoard {
      val expected: IndexedSeq[Position] = IndexedSeq(
          (0, 0), (0, 1), (0, 2),
          (1, 0), (1, 1), (1, 2),
          (2, 0), (2, 1), 
          (3, 0), (3, 1))
      
      val sort: Position => (Int, Int) = p => (p.x, p.y)
      
      board.attackedPositions.sortBy(sort) shouldBe expected.sortBy(sort)
    }
    
    "check is specified Position is a valid Position" in new EmptyBoard {
      board.isPosValid((1, 1)) shouldBe true
      board.isPosValid((6, 6)) shouldBe false
    }
    
    "check if specified Position is being attacked by existing Piece(s)" in new RooksBoard {
      board.isNotAttacked((2, 2)) shouldBe true
      board.isNotAttacked((0, 2)) shouldBe false
    }
    
    "check if specified Position is free" in new RooksBoard {
      board.isPosFree((0, 2)) shouldBe true
      board.isPosFree((1, 1)) shouldBe false
    }
    
    "check if attacking Position(s) of new Piece is valid" in new RooksBoard {
      board.isAttackPosValid(Rook, (2, 1)) shouldBe false
      board.isAttackPosValid(Rook, (2, 2)) shouldBe true
    }
    
    "check if Piece can be inserted on specified Position" in new RooksBoard {
      board.canBeInserted(Rook, (0, 0)) shouldBe false
      board.canBeInserted(Rook, (2, 2)) shouldBe true
    }
    
  }
  
}

