package pl.anestoruk.core

import org.scalatest._
import pl.anestoruk.models._

class LogicSpec extends WordSpec with Matchers with Logic {
  
  "deserialize method" should {
    
    "return valid Empty Board on proper input" in {
      val input = "   ;   ;   ;"
      val emptyBoard = Board(3, 3)
      deserialize(input) shouldBe Some(emptyBoard)
    }
    
    "return valid Board with Pieces on proper input" in {
      val input = "R  ; Q ;  N"
      val board = Board(Fields(Seq(
          Seq(Rook, EmptyField, EmptyField),
          Seq(EmptyField, Queen, EmptyField),
          Seq(EmptyField, EmptyField, Knight))))
      deserialize(input) shouldBe Some(board)
    }
    
  }
  
}