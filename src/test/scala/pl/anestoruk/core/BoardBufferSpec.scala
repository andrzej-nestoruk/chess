//package pl.anestoruk.core
//
//import java.io.ByteArrayOutputStream
//import java.nio.file.{ Files, Paths }
//import scala.collection.immutable.SortedSet
//import org.scalatest._
//import pl.anestoruk.models._
//import pl.anestoruk.utils.Config
//
//class BoardBufferSpec extends WordSpec with Config with Matchers with BeforeAndAfterAll {
//  
//  // config
//  val config = BoardBufferConfig(
//        settings.getString("directory"),
//        settings.getString("resultFile"),
//        settings.getString("tmpFile"),
//        settings.getInt("threshold"))
//  
//  // boards
//  val emptyBoard = Board(4, 3)
//  val fields = Fields(Seq(Seq(Rook, Queen, King)))
//  val board = Board(fields)
//  
//  /**
//   * Hides Console output
//   */
//  def run(f: Unit => Assertion) = {
//    val out = new ByteArrayOutputStream
//    Console.withOut(out) { f() }
//  }
//  
//  trait BoardBufferContext {
//    val buffer = new BoardBuffer(emptyBoard)
//  }
//  
//  trait FileBoardBufferContext {
//    val buffer = new FileBoardBuffer(emptyBoard, config)
//  }
//  
//  override def afterAll = {
//    import scala.collection.JavaConversions._
//    Files.list(Paths.get(config.directory)).iterator.toList.foreach { Files.delete(_) }
//    super.afterAll
//  }
//  
//  // *******************************************************************************************************************
//  
//  "BoardBuffer" when {
//  
//    "created" should {
//      "contain initial Board" in new BoardBufferContext {
//        buffer.resultBuffer shouldBe SortedSet(emptyBoard.toString)
//      }
//    }
//    
//    "+= method is invoked" should {
//      "add Board to temporary buffer" in new BoardBufferContext {
//        buffer += board
//        buffer.tmpBuffer.exists { _ == board.toString } shouldBe true
//      }
//    }
//    
//    "flush method is invoked" should {
//      "flush temporary buffer into result buffer" in new BoardBufferContext {
//        run { _ =>
//          val expected = SortedSet("RRR;QQQ", "KKK;NNN", " R ;B B")
//          buffer.tmpBuffer = expected
//          buffer.flush
//          buffer.resultBuffer shouldBe expected
//        }
//      }
//    }
//    
//    "foreach method is invoked" should {
//      "iterate over every element" in new BoardBufferContext {
//        run { _ =>
//          var result: SortedSet[String] = SortedSet.empty
//          val expected = SortedSet("RRR;QQQ", "KKK;NNN", " R ;B B")
//          buffer.resultBuffer = expected
//          buffer.foreach { result += _ }
//          result shouldBe expected
//        }
//      }
//    }
//  
//  }
//  
//  // *******************************************************************************************************************
//  
//  "FileBoardBuffer" when {
//    
//    "created" should {
//      "contain initial Board and create 'result' and 'tmp' files" in new FileBoardBufferContext {
//        Files.exists(buffer.resultPath) shouldBe true
//        val expected = emptyBoard.toString.getBytes
//        val result = Files.readAllBytes(buffer.resultPath) 
//        result shouldBe expected
//      }
//    }
//    
//    "+= method is invoked" should {
//      "add Board to temporary buffer" in new FileBoardBufferContext {
//        buffer += board
//        buffer.tmpBuffer.exists { _ == board.toString } shouldBe true
//      }
//    }
//    
//    "flush method is invoked" should {
//      "flush temporary buffer into result file" in new FileBoardBufferContext {
//        run { _ =>
//          val set = SortedSet("RRR;QQQ", "KKK;NNN", " R ;B B")
//          val expected = (set.mkString("\n") + "\n").getBytes
//          buffer.tmpBuffer = set
//          buffer.flush
//          val result = Files.readAllBytes(buffer.resultPath)
//          result shouldBe expected
//        }
//      }
//    }
//    
//    "foreach method is invoked" should {
//      "iterate over every element" in new FileBoardBufferContext {
//        run { _ =>
//          var set: SortedSet[String] = SortedSet.empty
//          val expected = (SortedSet("RRR;QQQ", "KKK;NNN", " R ;B B").mkString("\n") + "\n").getBytes
//          Files.write(buffer.resultPath, expected)
//          buffer.foreach { set += _ }
//          val result = (set.mkString("\n") + "\n").getBytes
//          result shouldBe expected
//        }
//      }
//    }
//    
//  }
//  
//}