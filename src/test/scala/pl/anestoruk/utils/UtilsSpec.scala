package pl.anestoruk

import java.io.{ ByteArrayInputStream, ByteArrayOutputStream }
import org.scalatest._
import pl.anestoruk.utils.Utils._

class UtilsSpec extends WordSpec with Matchers {
  
  "getInt method" should {
    
    "return Integer from Input" in {
      val input = new ByteArrayInputStream("6".getBytes)
      val out = new ByteArrayOutputStream
      Console.withOut(out) {
        Console.withIn(input) { 
          getInt("Podaj liczbę: ") shouldBe 6 
        }
      }
    }
    
    "return 0 if non-Integer was an Input" in {
      val input = new ByteArrayInputStream("ABC".getBytes)
      val out = new ByteArrayOutputStream
      Console.withOut(out) {
        Console.withIn(input) { 
          getInt("Podaj liczbę: ") shouldBe 0
        }
      }
    }
    
  }
  
  "factorial method" should {
    
    "calculate factorial for natural number" in {
      factorial(0) shouldBe 1
      factorial(1) shouldBe 1
      factorial(6) shouldBe (6 * 5 * 4 * 3 * 2)
    }
    
    "throw ArithmeticException for negative 'n' number" in {
      an [ArithmeticException] should be thrownBy factorial(-1)
    }
    
  }
  
  "variation method" should {
    
    "calculate variation for provided 'n' and 'k'" in {
      variation(5, 5) shouldBe (5 * 4 * 3 * 2)
      variation(10, 5) shouldBe (10 * 9 * 8 * 7 * 6)
    }
    
    "throw ArithmeticException for negative 'n' number and for 'k > n'" in {
      an [ArithmeticException] should be thrownBy variation(-5, 1)
      an [ArithmeticException] should be thrownBy variation(5, 6)
    }
    
  }
  
  "mergeAndSort method" should {
    
    "return combined Iterator that emits sorted values" in {
      val i1 = Iterator("ABC1", "BBB1", "CBA1", "DFA1", "HEH1")
      val i2 = Iterator("AAA2", "CCC2", "TEST", "XAX2", "XYZ2", "ZZA2")
      val i3 = Iterator("BBB3", "BAB3", "TEST")
      
      val expected = Seq(
          "AAA2", "ABC1", "BBB1", "BBB3", "BAB3", 
          "CBA1", "CCC2", "DFA1", "HEH1", "TEST", 
          "TEST", "XAX2", "XYZ2", "ZZA2")
      
      var result = Seq.empty[String]
      mergeAndSort(Seq(i1, i2, i3)).foreach { str => result = result :+ str }
      
      result shouldBe expected
    }
    
  }
  
}