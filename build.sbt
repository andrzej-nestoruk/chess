lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "pl.anestoruk",
      scalaVersion := "2.11.8",
      version := "0.1.0-SNAPSHOT"
    )),
    name := "Chess",
    libraryDependencies ++= Seq(
        "org.scalatest" %% "scalatest" % "3.0.1" % Test,
        "com.typesafe" % "config" % "1.3.1")
  )

fork in run := true
connectInput in run := true